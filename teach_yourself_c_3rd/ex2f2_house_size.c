/* Calculate the surface area of a house when the user inputs the
 * dimentions of each room. The program should ask the user how many
 * rooms the house has and the dimensions of each room. The program
 * should give the total size of the house.*/

#include <stdio.h>

int main()
{
	int room_num, i;
	double len, wid, house_size;

	printf("How many rooms are there in the house? ");
	scanf("%d", &room_num);

	for (i=0; i < room_num; i++){
		printf("Insert room's length in m: ");
		scanf("%lf", &len);

		printf("Insert room's width in m: ");
		scanf("%lf", &wid);

		house_size = house_size + len * wid;
	}
	printf("The total surface of the house is: %lf square metres.\n", house_size);

	return 0;
}

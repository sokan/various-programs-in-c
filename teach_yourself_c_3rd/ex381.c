/* Program to print only the odd numbers from 1 to 100, with continue
 * to exclude even numbers. */

#include <stdio.h>

int main()
{
	int i;
	int num;

	for(i = 1; i < 101; i++){
		if((i%2) == 0) continue;
		printf("%d  ", i);
	}

	return 0;

}

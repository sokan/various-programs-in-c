/* "Guess the magic number": the player has 10 tries to guess the magic number. 
 * If the number they guess is the magic number, print "RIGHT!". Otherwise the
 * program should say if the number of the user is bigger or smaller than the
 * magic number and ask for the user to re-enter another number. This should go 
 * on for 10 rounds or until the player finds the number. Optional: in the end
 * the program should mention how many tries it took for the player to figure 
 * out the magic number. */

#include <stdio.h>

int main()
{
	int magic = 89; //Magic number given by the programmer.
	int guess = 0; //Number the user gives each time. We also initialize.
	int i;

	for (i = 0; i < 10 && guess != magic; i++){
		
		printf("Guess the magic number: ");
		scanf("%d", &guess);

		if (guess == magic){
			printf("RIGHT! %d is the magic number!\n", magic);
		}
		
		else {
			printf("Try again!\n");
			if (guess < magic){
				printf("...but with a higher number!\n");
			}
			else {
				printf("...but with a lower number!\n");
			}
		}
	}

	return 0;
}

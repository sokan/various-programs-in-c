// A program with 2 functions

#include <stdio.h>

void func1(void); // prototype of func1() function

int main()
{
	printf("I ");
	func1();
	printf("C.\n");

	return 0;
}

void func1()
{
	printf("like ");
}

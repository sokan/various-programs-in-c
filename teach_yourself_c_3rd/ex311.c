/* Program that reads 10 letters. After they're read show which one
 * comes before all the given letters. a is the lowest/smaller and
 * z the higher/bigger. */

#include <stdio.h>

int main()
{	
	int i;
	char ch;
	char smallest = 'z';

	printf("Enter 10 letters without spaces:\n");
	
	for(i = 0; i < 10; i++){
		ch = getchar();
		if(ch < smallest){
			smallest = ch;
		}
	}

	printf("The letter closest to the first of the alphabet is: %c\n", smallest);
	

	return 0;
}

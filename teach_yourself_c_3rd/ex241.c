/* Program that prints number from 1 to 100 */

#include <stdio.h>

int main()
{
	int i;

	for (i = 1; i <= 101; i = i + 1){
		printf ("%d\t", i);
	}

	return 0;
}

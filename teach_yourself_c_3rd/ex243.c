/* Program that gives all divisors of given number. */

#include <stdio.h>

int main()
{
	int num, divisor;

	printf ("Give number to check: ");
	scanf ("%d", &num);

	for (divisor = 2; divisor <= num / 2; divisor = divisor + 1){
		if ((num%divisor) == 0){
			printf ("Your number can be divided with: %d\n", divisor);
		}
	}

	return 0;
}

#include <stdio.h>

int main()
{
	int num;

	printf("Enter integer: ");
	scanf("%d", &num);

	if (num%2 ==0) printf("It's even");
	else printf("It's odd");

	return 0;
}

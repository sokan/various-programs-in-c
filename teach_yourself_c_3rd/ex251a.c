/* Print all numbers from 1 to 100
 * by using a for loop and i++ 
 */

#include <stdio.h>

int main()
{
	int num, i;

	for (i = 1; i < 101; i++){
		printf ("%d\t", i);
	}
}

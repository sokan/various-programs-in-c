/* Program that calculates the surface of a circle, a square
 * or a triangle by using if-else-if. */

#include <stdio.h>

int main() {

	char ch;
	float rad, bas, hei;
	
	printf("Choose what surface you want to calcule (C)ircle, (S)quare or (T)riangle: ");
	ch = getchar();

	if (ch == 'C'){
		printf("Enter radius of the circle: ");
		scanf("%f", &rad);

		printf("The circle's surface is: %f.\n", 3.1416 * rad * rad);
	}

	else if(ch == 'T'){
		printf("Enter length of triangle's base: ");
		scanf("%f", &bas);

		printf("Enter height of the triangle: ");
		scanf("%f", &hei);

		printf("The surface of the triangle is %f.\n", (bas * hei) / 2);
	}

	else if(ch == 'S'){
		printf("Enter length of sqaure's base: ");
		scanf("%f", &bas);

		printf("Enter square's height: ");
		scanf("%f", &hei);

		printf("The surface of the square is %f.\n", bas * hei);
	}

	else printf("Enter one of the given options! Bye!\n");

	return 0;

}

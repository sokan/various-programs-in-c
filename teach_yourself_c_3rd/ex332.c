/* User inputs a number and we'll have a for loop that keeps running
 * until it reaches 0. After the countdown finishes the message should
 * output a sound with no other output in the screen. */

#include <stdio.h>

int main()
{
	int num;

	printf("Input a number: ");
	scanf("%d", &num);

	printf("And the countdown begins!\n");
	
	for( ; num; ){
		num--;
	}

	printf("\a"); //Makes the sound for output.

	return 0;

}

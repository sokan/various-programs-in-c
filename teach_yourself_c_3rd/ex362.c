/* Insert a character from keyboard and the program shall
 * print the corresponding ASCII code (number) as dots for 
 * each letter. Do this 10 times. */

#include<stdio.h>

int main()
{
	int i;
	char ch;

	for(i = 0; i < 10; i++){
		printf("Insert a character: ");
		ch = getchar();

		for( ; ch ; ch--){
			printf("%c", '.');
		}

		ch = getchar(); //Required in order to "empty" the ch variable from the buffer (In this case it only has "enter")	
		printf("\n\n");

	}

	return 0;

}

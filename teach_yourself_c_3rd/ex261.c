/* Output a table of numbers. Each line must have 3 number
 * inputs: number, number squared, number cubed. Start from 
 * 1 and end on 10. Use a for loop. */

#include <stdio.h>

int main()
{
	int i;

	for (i = 1; i < 11; i++){
		printf ("%d\t%d\t%d\n", i, i*i, i*i*i);
	}

	return 0;
}

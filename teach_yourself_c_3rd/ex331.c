/* Program that calculates the required time if given speed and distance. 
 * The user must input how many different calculations (based on speed or
 * distances) they want to do. */

#include <stdio.h>

int main()
{

	int times;
	float dist, speed;

	printf("How many calculations do you want to do? ");
	scanf("%d", &times);

	for(; times; times = times - 1){
		printf("\nEnter speed (km/h): ");
		scanf("%f", &speed);

		printf("Enter distance (km): ");
		scanf("%f", &dist);

		printf("The time required with these data is %f hours\n", dist / speed);

	}

	return 0;

}

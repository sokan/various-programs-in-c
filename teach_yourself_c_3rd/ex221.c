#include <stdio.h>

int main()
{
	int num1, num2, inp;

	printf("Enter first number: ");
	scanf("%d", &num1);

	printf("Enter second number: ");
	scanf("%d", &num2);

	printf("Enter 0 for addition and 1 for multiplication: ");
	scanf("%d", &inp);

	if (inp == 0) printf("Result is: %d", num1 + num2);
	else printf("Result is: %d", num1 * num2);

	return 0;
}

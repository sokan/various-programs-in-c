/* Program that shows the ASCII codes of characters from A to Z and a to z. */

#include <stdio.h>

int main()
{
	char ch;
	
	printf("a to z in ASCII is:\n");

	for(ch = 'a'; ch <= 'z'; ch++){
		printf("%d ", ch);
	}
	
	printf("\nA to Z in ASCII is:\n");

	for(ch = 'A'; ch <= 'Z'; ch++){
		printf("%d ", ch);
	}

	return 0;

}

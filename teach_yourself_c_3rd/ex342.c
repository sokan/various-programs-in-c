/* A program that decodes messages that have been coded by the change of
 * a letter to it's next (e.g. A -> B). */

#include <stdio.h>

int main()
{

	char ch;

	printf("Enter your coded message: ");
	ch = getchar();

	while(ch != '\r' && ch != '\n') {
		printf("%c", ch - 1);
		ch = getchar();
	}

	return 0;

}

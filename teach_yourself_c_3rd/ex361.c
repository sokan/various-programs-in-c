/* Program that finds all prime numbers between 2 and 1000 */

#include <stdio.h>

int main()
{
	int num, j, flag;
	
	printf("All prime numbers from 2 to 1000 are: \n");

	for(num = 2; num <= 1000; num++){
		flag = 0;
		for(j = 2; j <= num/2; j++){ //This for loop is used for checking prime numbering. when our number gets divided by 2, then it's no longer a prime, so no use checking it. We start by 2 because * % 1 = 0, so it'll aways be flag = 1. 
			if(num%j == 0){ // Checks how many times the number is divided with 0 remain.
				flag = 1;
				break;
			}
		}
		if(flag == 0){ //If the number can't be divided with 0 remains.
				printf("%d  ", num);
		}
	}

	return 0;
}

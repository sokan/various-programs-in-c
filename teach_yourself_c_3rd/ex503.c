/* Program that takes a long, short and double value as input
 * and then prints them on screen */

#include <stdio.h>

int main()
{
	long l;
	short s;
	double d;

	printf("Enter a long value: ");
	scanf("%ld", &l);

	printf("Enter a short value: ");
	scanf("%hd", &s);

	printf("Enter a double value: ");
	scanf("%lf", &d);

	printf("%ld\n", l);
	printf("%hd\n", s);
	printf("%lf\n",d);

	return 0;
}

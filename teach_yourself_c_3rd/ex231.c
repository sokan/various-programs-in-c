#include <stdio.h>

int main()
{
	int num1, num2, op;
	
	printf("Choose 1 for addition or 2 for subtraction: ");
	scanf("%d", &op);
	
	if (op == 1)
	{
		printf("Now choose an integer: ");
		scanf("%d", &num1);

		printf("And one more: ");
		scanf("%d", &num2);

		printf("The addition is: %d", num1 + num2);
	}

	else 
	{
		printf("Now choose an integer: ");
		scanf("%d", &num1);

		printf("Now another: ");
		scanf("%d", &num2);

		printf("The subtraction is: %d", num1 - num2);
	}

	return 0;
}

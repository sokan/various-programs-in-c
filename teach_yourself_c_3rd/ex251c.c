/* Program that gives all divisors of a, given by the 
 * user, number by using a for loop and i++ */

#include <stdio.h>

int main()
{
	int num, i;
	
	printf ("Enter number to check: ");
	scanf ("%d",&num);

	for (i = 2; i < (num/2) + 1; i++){
		if ((num%i == 0)){
			printf ("%d ", i);
		}
	}

	return 0;
}

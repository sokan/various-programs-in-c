// My weight on the moon.

#include <stdio.h>

int main()
{
	float my_weight;
	my_weight = 85.7;

	printf("My weight on the moon is %f: Kg\n", 0.17 * my_weight);

	return 0;
}

/* Program that calculates the required time if given speed and distance. 
 * The user must input how many different calculations (based on speed or
 * distances) they want to do. This time with a while loop. */

#include <stdio.h>

int main()
{

	int times;
	float dist, speed;

	printf("How many calculations do you want to do? ");
	scanf("%d", &times);

	while(times){
		printf("\nEnter speed (km/h): ");
		scanf("%f", &speed);

		printf("Enter distance (km): ");
		scanf("%f", &dist);

		printf("The time required with these data is %f hours\n", dist / speed);

		times = times - 1;
	}

	return 0;

}

#include <stdio.h>

int main(void)
{
	char ch;
	float f;
	double d;

	ch = 'x';
	f = 100.123;
	d = 123.009;

	printf("ch is %c, ", ch);
	printf("d is %f, ", d);
	printf("f is %f\n", f);

	return 0;
}

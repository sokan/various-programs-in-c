/* A program that starts from 1 and finishes at 1000. Ask the user
 * the step for which he'd like the program to run (it's different
 * from te book exersise). */

#include <stdio.h>

int main()
{
	int num, i;
	
	printf("How bit a step do you want? \n");
	scanf("%d", &num);

	for(i = 1; i < 1001; i = i + num) printf("%d ", i);

	return 0;

}

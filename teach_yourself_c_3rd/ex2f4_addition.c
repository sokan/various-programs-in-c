/* An addition program that checks if the user did the addition
 * correctly or not. It will also keep the number the right and
 * wrong answers given and give these numbers when it finishes
 * (namely when the user did the addition correctly) */

#include <stdio.h>

int main()
{
	int answer, count;
	int right, wrong;

	right = 0;
	wrong = 0;

	for(count = 1; count < 11; count++){
		printf("What is %d + %d? ", count, count);
		scanf("%d", &answer);

		if(answer == count + count){
			printf("Correct!\n", right = right + 1);
		}
		else{
			printf("Nope! The right answer is %d.\n", count+count, wrong = wrong + 1);
		}
	}

	printf("You gave %d right answers and %d wrong ones.\n", right, wrong);

	return 0;
}

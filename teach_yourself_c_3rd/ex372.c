/* The right amount of tip based on the amount of the bill.
 * Start by 1$ bill and stop at 100$ bill, with a step of
 * 1$. Calculate the tips based on 10%, 15% and 20%. After
 * each line sk the user if they want to keep on going. If
 * the user adds a negative input end the program. */

#include <stdio.h>

int main()
{
	int i;
	float bill;
	char ch;
	
	for(i = 1.0; i < 101.0; i = i + 1){
		printf("For a bill of %d$ the tips are %f$ (10%%), %f$ (15%%) and %f$ (20%%)", i, i * 0.1, i * 0.15, i * 0.20);
		
		printf("Is the bill higher than this (Y/N)? ");
		ch = getchar();

		printf("\n");

		if(ch == 'N') break;

		while((ch = getchar()) != ('\n')) {}
	}


	return 0;

}

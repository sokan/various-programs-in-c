/* Program to count the number of letters, digits and common punctuation
 * symbols that the user inputs. The input must stop when the user presses
 * ENTER. Use `switch` to categorize the characters into punctuations,
 * numerical digits and letters. When the programs ends/stops, mention the
 * number of characters in each category. */

#include <stdio.h>

int main()
{
	int number, letter, symbol;
	char ch;
	
	number = letter = symbol = 0;

	printf("Enter a sequence of common puncuation symbols, letters and digits and when done press ENTER: \n");
	do{
	ch = getchar();
	
	switch(ch){
		case '1':
		case '2':
		case '3':
		case '4':
		case '5':
		case '6':
		case '7':
		case '8':
		case '9':
		case '0':
			number++;
			break;
		case '.':
		case ',':
		case '?':
		case '~':
		case '!':
		case ';':
		case ':':
		case '@':
		case '#':
		case '%':
			symbol++;
			break;
		case '\n': 		//necessary in order to avoid counting \n anywhere else.
			break;
		default:
			letter++; 
		}
	}while(ch != '\n');

	printf("Your sequence has %d letter(s), %d punctunation symbol(s) and %d number(s).\n", letter, symbol, number);

	return 0;
}

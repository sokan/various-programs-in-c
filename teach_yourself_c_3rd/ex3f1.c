/* Program that will read chracters from keyboard and will check for \t, \n and backspace
 * characters, by using a switch. When it recognises a chracter like that, the program 
 * will report who that is by using a word, e.g. when the user presses TAB the program 
 * will print out "tab". Ask of the user to input "q" to quit the program. */

#include <stdio.h>

int main()
{
	char ch;

	printf("Enter characters and some magic awaits you! Press Q to quit.\n");

	do{
		ch = getchar();
		switch(ch){
			case '\b': 
				printf("Backspace!!\n");
				break;
			case '\t': 
				printf("TAB\n");
				break;
			case '\n':
				printf("Enter\n");
		}
	}while(ch != 'Q');

	return 0;

}
// Program needs some finetuning due to that "issue" of getchar() that requires ENTER to work...

/* Program that prints the numbers from 17 to 100
 * that are perfectly divided by 17 */

#include <stdio.h>

int main()
{
	int i;

	for (i = 18; i <= 101; i = i + 1){
		if (i%17 == 0) printf ("%d\t", i);
	}

	return 0;
}

/* User input an integer. Use a for loop to count from
 * that integer up to 0, with the output at a newline 
 * on each count. When program hits 0, get the program
 * to output a sound. */

#include <stdio.h>

int main()
{
	int num, i;

	printf ("Enter integer: ");
	scanf ("%d", &num);

	for (i = num; i > 0 ; i--){
		printf ("%d\n", i);
		printf ("\a"); // Computer makes a sound (provided the kernel options allow it).
	}

	return 0;
}

/* Print all numbers from 17 to 100 that are divided 
 * perfectly with 17 by using a for loop and i++ */

#include <stdio.h>

int main()
{
	int num, i;

	for (i = 1; i < 101; i++){
		if ((i%17 == 0)){
			printf ("%d\t", i);
		}
	}

	return 0;
}

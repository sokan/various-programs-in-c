/* Check if the user inputs one of the 5 options given by the program. */

#include <stdio.h>

int main()
{

	int choose;

	printf("Mailing list menu:\n\n1. Enter addresses\n2. Delete address\n3. Search the lsit\n4. Print the list\n5. Quit\n");

	do{
		printf("Choose from list (1-5): ");
		scanf("%d", &choose);

	} while(choose <= 4 && choose >= 1);

	return 0;
}

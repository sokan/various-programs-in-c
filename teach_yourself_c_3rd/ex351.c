/* Program that changes galons to litres (1 galon = 3.7841 lt). 
 * Using a do loop for repeatability of inputs. */

#include <stdio.h>

int main()
{

	float num;
	
	printf("Enter how many galons you want to change to litres (0 to end the program): ");
	scanf("%f", &num);

	do{
		printf("\n%f gallons are %f litres\n", num, num * 3.7841);

		printf("How many galons you want to calculate to litres (0 to end the program): ");
		scanf("%f", &num);

	} while(num != 0);

	return 0;

}
